﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TYStudio;

namespace TYStudio.UI.AdminTools.Controllers
{
    //[Authorize(Roles = "系统管理员")]
    [AllowAnonymous]
    [InitializeSimpleMembership]
    public class PermissionController : Controller
    {
        private TYStudioMembershipContext db = new TYStudioMembershipContext();

        public ActionResult Index()
        {
            return View(db.Permissions.ToList());
        }

        //
        // GET: /Permission/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Permission/Create

        [HttpPost]
        public ActionResult Create(Permission permission)
        {
            if (ModelState.IsValid)
            {
                if (db.Permissions.SingleOrDefault(e => e.PermissionName == permission.PermissionName) == null)
                {
                    db.Permissions.Add(permission);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["ErrorMessage"] = "权限名称已存在";
                }
            }

            return View(permission);
        }

        //
        // GET: /Permission/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Permission permission = db.Permissions.Find(id);
            if (permission == null)
            {
                return HttpNotFound();
            }
            return View(permission);
        }

        //
        // POST: /Permission/Edit/5

        [HttpPost]
        public ActionResult Edit(Permission permission)
        {
            if (ModelState.IsValid)
            {
                if (db.Permissions.SingleOrDefault(e => e.PermissionName == permission.PermissionName) == null)
                {
                    db.Entry(permission).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["ErrorMessage"] = "权限名称已存在";
                }
                
            }
            return View(permission);
        }

        //
        // GET: /Permission/Delete/5

        public ActionResult Delete(int id)
        {
            Permission permission = db.Permissions.Find(id);
            db.Permissions.Remove(permission);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}