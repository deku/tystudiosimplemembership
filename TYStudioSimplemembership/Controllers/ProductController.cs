using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TYStudio;

namespace TYStudioSimplemembership.Controllers
{
    /// <summary>
    /// 这是一个产品权限的测试Controller
    /// </summary>
    /// 
    [Authorize]
    [InitializeSimpleMembership]
    public class ProductController : Controller
    {
        [TYStudioAuthorize(permission="查询产品")]
        public ActionResult Index()
        {
            return View();
        }

        [TYStudioAuthorize(permission="添加产品")]
        public ActionResult Create()
        {
            return View();
        }

        [TYStudioAuthorize(permission="编辑产品")]
        public ActionResult Edit()
        {
            return View();
        }

        [TYStudioAuthorize(permission="删除产品")]
        public ActionResult Delete()
        {
            return View();
        }

    }
}
