﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;

namespace TYStudio
{
    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        [Display(Name="ID")]
        public int UserId { get; set; }

        [Display(Name = "用户名")]
        [StringLength(250)]
        public string UserName { get; set; }

        [Display(Name = "邮件地址")]
        [StringLength(250)]
        public string Email { get; set; }
    }
}
